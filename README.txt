Flexible Outbrain Integration
-----------------------------

This module integrates Drupal with Outbrain - http://www.outbrain.com/

You need to register with outbrian. The module provides the ability to 
create multiple blocks to be populated with Outbrain content.

INSTALLATION
============

- Enable the module
- Set permissions on the permission page
(admin/people/permissions#module-flexible_outbrain_integration)
if you want other roles to administer Ourbrain
- Configure outbrain at admin/config/outbrain_integration
- Display outbrain blocks on your pages (admin/structure/block)
