<?php

/**
 * @file
 * Contains the administrative functions of the outbrain integration module.
 *
 * This file is included by the outbrain integration module, and includes the
 * settings form.
 */

/**
 * Page callback:Used to create the admin form to configure the outbrain blocks.
 */
function flexible_outbrain_integration_settings_form($form, &$form_state) {

  $blocks = variable_get('ob_blocks', array());
  if (count($blocks) > 0 && empty($form_state['num_names'])) {
    $form_state['num_names'] = count($blocks);
  }

  $form['#tree'] = TRUE;
  $form['ob_blocks'] = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#title' => t('Outbrain Block settings'),
    '#prefix' => '<div id="ob_blocks">',
    '#suffix' => '</div>',
  );

  if (empty($form_state['num_names'])) {
    $form_state['num_names'] = 1;
  }

  for ($i = 1; $i <= $form_state['num_names']; $i++) {
    $form['ob_blocks'][$i] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#title' => t('Block @num Settings', array('@num' => $i)),
    );

    $title = !empty($blocks[$i]['title']) ? $blocks[$i]['title'] : '';
    $widget_id = !empty($blocks[$i]['widget_id']) ? $blocks[$i]['widget_id'] : '';
    $template = !empty($blocks[$i]['template']) ? $blocks[$i]['template'] : '';

    $form['ob_blocks'][$i]['title'] = array(
      '#type' => 'textfield',
      '#title' => t('Ads Block Title'),
      '#default_value' => $title,
      '#description' => t('The title you want to be displayed on the block. Use &lt;none&gt; if no title is desired.'),
    );
    $form['ob_blocks'][$i]['widget_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Ads Data Widget ID'),
      '#default_value' => $widget_id,
      '#description' => t('Data Widget ID. Example: AR_1'),
    );
    $form['ob_blocks'][$i]['template'] = array(
      '#type' => 'textfield',
      '#title' => t('Ads Data OB Template'),
      '#default_value' => $template,
      '#description' => t('OB Template. Example: CUSTOM_TEMPLATE'),
    );
  }

  $form['ob_blocks']['add_more_block'] = array(
    '#type' => 'submit',
    '#value' => t('Add More'),
    '#submit' => array('flexible_outbrain_integration_add_more_block'),
    '#ajax' => array(
      'callback' => 'flexible_outbrain_integration_add_more_callback',
      'wrapper' => 'ob_blocks',
    ),
  );

  if ($form_state['num_names'] > 1) {
    $form['ob_blocks']['remove_block'] = array(
      '#type' => 'submit',
      '#value' => t('Remove'),
      '#submit' => array('flexible_outbrain_integration_add_more_remove'),
      '#ajax' => array(
        'callback' => 'flexible_outbrain_integration_add_more_callback',
        'wrapper' => 'ob_blocks',
      ),
    );
  }
  return system_settings_form($form);
}

/**
 * Callback for ajax-enabled buttons.
 */
function flexible_outbrain_integration_add_more_callback($form, $form_state) {
  return $form['ob_blocks'];
}

/**
 * Submit handler for Add more button.
 */
function flexible_outbrain_integration_add_more_block($form, &$form_state) {
  $form_state['num_names']++;
  $form_state['rebuild'] = TRUE;
}

/**
 * Submit handler for remove button.
 */
function flexible_outbrain_integration_add_more_remove($form, &$form_state) {
  if ($form_state['num_names'] > 1) {
    $form_state['num_names']--;
  }
  $form_state['rebuild'] = TRUE;
}
